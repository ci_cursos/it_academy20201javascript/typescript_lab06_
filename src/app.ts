import express from 'express';
import { router } from './routers/biblioteca.routes';
import errorHandler from 'errorhandler';

const app = express();
app.set('port', process.env.PORT);
app.use('/api',router);
if (process.env.NODE_ENV === 'development') {
    app.use(errorHandler());
}
export default app;